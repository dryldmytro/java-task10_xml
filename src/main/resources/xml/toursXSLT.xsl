<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
        xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/">
<html>
    <body>
        <h2>Tours vouchers</h2>
        <table border="1">
            <tr bgcolor="#9acd32">
                <th rowspan="2" style="width:250px">Country</th>
                <th rowspan="2" style="width:250px">Voucher type</th>
                <th rowspan="2" style="width:250px">Days</th>
                <th rowspan="2" style="width:250px">Transport</th>
                <th colspan="5" class="first" style="width:250px">Hotel Characteristic</th>
                <th rowspan="2" style="width:350">Price</th>
            </tr>
            <tr bgcolor="#9acd32">
                <td class="first" style="width:250px">stars</td>
                <td class="first" style="width:250px">food</td>
                <td class="first" style="width:250px">room</td>
                <td class="first" style="width:250px">WI-FI</td>
                <td class="first" style="width:250px">TV</td>
            </tr>

            <xsl:for-each select="tourist_vouchers/voucherType">
                <tr>
                    <td><xsl:value-of select="country"/></td>
                    <td><xsl:value-of select="@type"/></td>
                    <td><xsl:value-of select="days"/></td>
                    <td><xsl:value-of select="transport"/></td>
                    <td><xsl:value-of select="hotelCharacteristic/stars"/></td>
                    <td><xsl:value-of select="hotelCharacteristic/food"/></td>
                    <td><xsl:value-of select="hotelCharacteristic/room"/></td>
                    <td><xsl:choose>
                        <xsl:when test="hotelCharacteristic/WI-FI='true'">
                            <xsl:text>Yes</xsl:text>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:text>No</xsl:text>
                        </xsl:otherwise>
                    </xsl:choose></td>
                    <td><xsl:choose>
                        <xsl:when test="hotelCharacteristic/TV='true'">
                            <xsl:text>Yes</xsl:text>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:text>No</xsl:text>
                        </xsl:otherwise>
                    </xsl:choose></td>
                    <td><xsl:value-of select="price"/>
                        <xsl:text>$</xsl:text>
                    </td>
                </tr>
            </xsl:for-each>
        </table>
    </body>
</html>
    </xsl:template>
</xsl:stylesheet>