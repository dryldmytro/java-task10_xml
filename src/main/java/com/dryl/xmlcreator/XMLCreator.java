package com.dryl.xmlcreator;

import com.dryl.model.Voucher;
import com.dryl.model.VoucherComparator;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Text;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.FileOutputStream;
import java.util.Collections;
import java.util.List;

public class XMLCreator {
    public static void createXML(List<Voucher> list){
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document document = builder.newDocument();
            Element root = document.createElement("tourist_vouchers");
            document.appendChild(root);
            Collections.sort(list, new VoucherComparator());
            for (Voucher v:list) {
                Element voucherType = document.createElement("voucherType");
                voucherType.setAttribute("type",v.getVoucherType());
                root.appendChild(voucherType);

                Element country = document.createElement("country");
                Text countryText = document.createTextNode(v.getCountry());
                country.appendChild(countryText);

                Element days = document.createElement("days");
                Text daysText = document.createTextNode(""+v.getDays());
                days.appendChild(daysText);

                Element transport = document.createElement("transport");
                Text transportText = document.createTextNode(v.getTransport());
                transport.appendChild(transportText);

                Element price = document.createElement("price");
                Text priceText = document.createTextNode(""+v.getPrice());
                price.appendChild(priceText);

                Element hotel = document.createElement("hotelCharacteristic");

                Element stars = document.createElement("stars");
                Text starsText = document.createTextNode(""+v.getHotel().getStars());
                stars.appendChild(starsText);
                hotel.appendChild(stars);

                Element food = document.createElement("food");
                Text foodText = document.createTextNode(v.getHotel().getFood());
                food.appendChild(foodText);
                hotel.appendChild(food);

                Element room = document.createElement("room");
                Text roomText = document.createTextNode(v.getHotel().getRoom());
                room.appendChild(roomText);
                hotel.appendChild(room);

                Element wifi = document.createElement("WI-FI");
                Text wifiText = document.createTextNode(""+v.getHotel().isWifi());
                wifi.appendChild(wifiText);
                hotel.appendChild(wifi);

                Element tv = document.createElement("TV");
                Text tvText = document.createTextNode(""+v.getHotel().isTv());
                tv.appendChild(tvText);
                hotel.appendChild(tv);

                voucherType.appendChild(country);
                voucherType.appendChild(days);
                voucherType.appendChild(transport);
                voucherType.appendChild(hotel);
                voucherType.appendChild(price);
            }

            Transformer t = TransformerFactory.newInstance().newTransformer();
            t.setOutputProperty(OutputKeys.INDENT, "yes");
            t.transform(new DOMSource(document), new StreamResult(new FileOutputStream("temp.xml")));
        }catch (Exception e){}
    }
}
