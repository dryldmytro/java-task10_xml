package com.dryl.model;

public class Voucher {
    private String voucherType;
    private int days;
    private int price;
    private String country;
    private String transport;
    private Hotel hotel;

    public Hotel getHotel() {
        return hotel;
    }

    public void setHotel(Hotel hotel) {
        this.hotel = hotel;
    }

    public String getTransport() {
        return transport;
    }

    public void setTransport(String transport) {
        this.transport = transport;
    }

    public String getVoucherType() {
        return voucherType;
    }

    public void setVoucherType(String voucherType) {
        this.voucherType = voucherType;
    }

    public int getDays() {
        return days;
    }

    public void setDays(int days) {
        this.days = days;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @Override
    public String toString() {
        return "Voucher{" +
                "voucherType='" + voucherType + '\'' +
                ", days=" + days +
                ", price=" + price +
                ", country='" + country + '\'' +
                ", transport='" + transport + '\'' +
                ", hotel=" + hotel +
                '}';
    }
}
