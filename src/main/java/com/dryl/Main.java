package com.dryl;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;

public class Main {
    public static void main(String[] args) throws ParserConfigurationException, IOException, SAXException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document document = builder.parse(new File("/Users/dmytrodrul/IdeaProjects/xml/src/main/resources/xml/tours.xml"));
        Element element = document.getDocumentElement();
        System.out.println(element.getTagName());
        NodeList nodeList = element.getChildNodes();
        printElements(nodeList);
    }
    static void printElements(NodeList nodeList){
        for (int i = 0; i < nodeList.getLength(); i++) {
            if (nodeList.item(i) instanceof Element){
                String value = "";
                if (!nodeList.item(i).getTextContent().trim().isEmpty()&& !((Text)nodeList.item(i).getFirstChild()).getData().trim().isEmpty()
                && !((Text)nodeList.item(i).getFirstChild()).getData().trim().equals("\n")){
                    Text text = (Text) nodeList.item(i).getFirstChild();
                    value+="="+text.getData().trim();
                    System.out.println(value);
                }
//                System.out.println(((Element) nodeList.item(i)).getTagName());
//                if (((Element) nodeList.item(i)).hasAttribute("type")){
//                    System.out.println(((Element) nodeList.item(i)).getAttribute("type"));
//                }
                if (nodeList.item(i).hasChildNodes()){
                    printElements(nodeList.item(i).getChildNodes());
                }
            }
        }
    }
}
