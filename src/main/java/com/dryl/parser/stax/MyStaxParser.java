package com.dryl.parser.stax;

import com.dryl.model.Hotel;
import com.dryl.model.Voucher;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

public class MyStaxParser {
    public static List<Voucher> parse(File file) throws FileNotFoundException, XMLStreamException {
        List<Voucher> list = new ArrayList<>();
         Voucher voucher=null;
         Hotel hotel=null;
        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        XMLEventReader reader = xmlInputFactory.createXMLEventReader(new FileInputStream(file));
        while (reader.hasNext()){
            XMLEvent event = reader.nextEvent();
            if (event.isStartElement()){
                StartElement startElement = event.asStartElement();
                String name = startElement.getName().getLocalPart();
                switch (name){
                    case "voucherType":
                       voucher = new Voucher();
                        Attribute attribute = startElement.getAttributeByName(new QName("type"));
                        if (attribute!=null) {
                            voucher.setVoucherType(attribute.getValue());
                        }
                        break;
                    case "country":
                        event = reader.nextEvent();
                        assert voucher != null;
                        voucher.setCountry(event.asCharacters().getData());
                        break;
                    case "days":
                        event = reader.nextEvent();
                        assert voucher!=null;
                        voucher.setDays(Integer.parseInt(event.asCharacters().getData()));
                        break;
                    case "transport":
                        event = reader.nextEvent();
                        assert voucher!=null;
                        voucher.setTransport(event.asCharacters().getData());
                        break;
                    case "hotelCharacteristic":
                        event = reader.nextEvent();
                        hotel = new Hotel();
                        break;
                    case "stars":
                        event = reader.nextEvent();
                        assert hotel !=null;
                        hotel.setStars(Integer.parseInt(event.asCharacters().getData()));
                        break;
                    case "food":
                        event = reader.nextEvent();
                        assert hotel !=null;
                        hotel.setFood(event.asCharacters().getData());
                        break;
                    case "room":
                        event = reader.nextEvent();
                        assert hotel !=null;
                        hotel.setRoom(event.asCharacters().getData());
                        break;
                    case "WI-FI":
                        event = reader.nextEvent();
                        assert hotel !=null;
                        hotel.setWifi(Boolean.parseBoolean(event.asCharacters().getData()));
                        break;
                    case "TV":
                        event = reader.nextEvent();
                        assert hotel !=null;
                        hotel.setTv(Boolean.parseBoolean(event.asCharacters().getData()));
                        break;
                    case "price":
                        event = reader.nextEvent();
                        assert voucher!=null;
                        voucher.setPrice(Integer.parseInt(event.asCharacters().getData()));
                        break;
                }
            }
            if (event.isEndElement()){

                EndElement endElement = event.asEndElement();
                if (endElement.getName().getLocalPart().equals("voucherType")){
                    list.add(voucher);
                }
                if (endElement.getName().getLocalPart().equals("hotelCharacteristic")){
                    voucher.setHotel(hotel);
                }
            }
        }
        return list;
    }
}
