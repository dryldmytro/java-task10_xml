package com.dryl.parser.stax;

import com.dryl.model.Voucher;
import com.dryl.parser.sax.MySAXParser;

import javax.xml.stream.XMLStreamException;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;

public class Main {
    public static void main(String[] args) throws FileNotFoundException, XMLStreamException {
        File file = new File("/Users/dmytrodrul/IdeaProjects/xml/src/main/resources/xml/tours.xml");
        List<Voucher> vouchers = MyStaxParser.parse(file);
        System.out.println(vouchers);
    }
}
