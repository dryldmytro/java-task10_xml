package com.dryl.parser.dom;

import com.dryl.model.Hotel;
import com.dryl.model.Voucher;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.ArrayList;
import java.util.List;

public class DOMParser {
    public static List<Voucher> parse(Document document){
        List<Voucher> vouchers = new ArrayList<>();
        NodeList nodeList = document.getElementsByTagName("voucherType");
        for (int i = 0; i < nodeList.getLength(); i++) {
            Voucher voucher = new Voucher();
            Hotel hotel;

            Element element = (Element) nodeList.item(i);
            voucher.setVoucherType(element.getAttribute("type"));
            voucher.setDays(Integer.parseInt(element.getElementsByTagName("days").item(0).getTextContent()));
            voucher.setPrice(Integer.parseInt(element.getElementsByTagName("price").item(0).getTextContent()));
            voucher.setCountry(element.getElementsByTagName("country").item(0).getTextContent());
            voucher.setTransport(element.getElementsByTagName("transport").item(0).getTextContent());
            hotel = getHotel(element.getElementsByTagName("hotelCharacteristic"));
            voucher.setHotel(hotel);
            vouchers.add(voucher);
        }
        return vouchers;
    }
    public static Hotel getHotel(NodeList nodeList){
        Hotel hotel = new Hotel();
        if (nodeList.item(0).getNodeType()==Node.ELEMENT_NODE){
            Element element = (Element) nodeList.item(0);
            if (element.getElementsByTagName("TV").item(0)!=null){
            hotel.setTv(Boolean.parseBoolean(element.getElementsByTagName("TV").item(0).getTextContent()));}
            if (element.getElementsByTagName("WI-FI").item(0)!=null){
            hotel.setWifi(Boolean.parseBoolean(element.getElementsByTagName("WI-FI").item(0).getTextContent()));}
            if (element.getElementsByTagName("room").item(0)!=null){
            hotel.setRoom(element.getElementsByTagName("room").item(0).getTextContent());}
            if (element.getElementsByTagName("food").item(0)!=null){
            hotel.setFood(element.getElementsByTagName("food").item(0).getTextContent());}
            if (element.getElementsByTagName("stars").item(0)!=null){
            hotel.setStars(Integer.parseInt(element.getElementsByTagName("stars").item(0).getTextContent()));}
        }
        return hotel;
    }
}
