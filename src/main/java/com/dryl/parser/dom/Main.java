package com.dryl.parser.dom;

import com.dryl.model.Voucher;
import com.dryl.model.VoucherComparator;
import com.dryl.xmlcreator.XMLCreator;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.List;

public class Main {
    private static DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    private static DocumentBuilder builder;
    public static void main(String[] args) throws ParserConfigurationException, IOException, SAXException {
        File file = new File("/Users/dmytrodrul/IdeaProjects/xml/src/main/resources/xml/tours.xml");
        builder = factory.newDocumentBuilder();
        Document document = builder.parse(file);
        List<Voucher> list = DOMParser.parse(document);
        XMLCreator creator = new XMLCreator();
        creator.createXML(list);
    }
}
