package com.dryl.parser.sax;

import com.dryl.model.Voucher;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.List;

public class Main {
    public static void main(String[] args) throws IOException, SAXException, ParserConfigurationException {
        File file = new File("/Users/dmytrodrul/IdeaProjects/xml/src/main/resources/xml/tours.xml");
        List<Voucher> vouchers = MySAXParser.parse(file);
        System.out.println(vouchers);
    }
}
