package com.dryl.parser.sax;

import com.dryl.model.Hotel;
import com.dryl.model.Voucher;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.List;

public class MySaxHandler extends DefaultHandler {
    private List<Voucher> vouchers = new ArrayList<>();

    private boolean vCountry = false;
    private boolean vDays = false;
    private boolean vTransport = false;
    private boolean vHotel = false;
    private boolean hStars = false;
    private boolean hFood = false;
    private boolean hRoom = false;
    private boolean hWIFI = false;
    private boolean hTV = false;
    private boolean vPrice = false;
    private Voucher voucher = null;
    private Hotel hotel= null;

    public List<Voucher> getVouchers() {
        return this.vouchers;
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        if (qName.equalsIgnoreCase("voucherType")){
           String voucherN= attributes.getValue("type");
           voucher = new Voucher();
           voucher.setVoucherType(voucherN);
        }else if (qName.equalsIgnoreCase("hotelCharacteristic")){
            hotel=new Hotel();
        }
        else if (qName.equalsIgnoreCase("country")){vCountry=true;}
        else if (qName.equalsIgnoreCase("days")){vDays=true;}
        else if (qName.equalsIgnoreCase("transport")){vTransport=true;}
        else if (qName.equalsIgnoreCase("stars")){hStars=true;}
        else if (qName.equalsIgnoreCase("food")){hFood=true;}
        else if (qName.equalsIgnoreCase("room")){hRoom=true;}
        else if (qName.equalsIgnoreCase("WI-FI")){hWIFI=true;}
        else if (qName.equalsIgnoreCase("TV")){hTV=true;}
        else if (qName.equalsIgnoreCase("price")){vPrice=true;}
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (qName.equalsIgnoreCase("voucherType")){
            vouchers.add(voucher);
        }else if (qName.equalsIgnoreCase("hotelCharacteristic")){
            voucher.setHotel(hotel);
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
    if (vCountry){
        voucher.setCountry(new String(ch,start,length));
        vCountry=false;
    }else if (vDays){
        voucher.setDays(Integer.parseInt(new String(ch,start,length)));
        vDays=false;
    }else if (vTransport){
        voucher.setTransport(new String(ch,start,length));
        vTransport=false;
    }else if (hStars){
        hotel.setStars(Integer.parseInt(new String(ch,start,length)));
        hStars=false;
    }else if (hFood){
        hotel.setFood(new String(ch,start,length));
        hFood=false;
    }else if (hRoom){
        hotel.setRoom(new String(ch,start,length));
        hRoom=false;
    }else if (hWIFI){
        hotel.setWifi(Boolean.parseBoolean(new String(ch,start,length)));
        hWIFI=false;
    }else if (hTV){
        hotel.setTv(Boolean.parseBoolean(new String(ch,start,length)));
        hTV=false;
    }else if (vPrice){
        voucher.setPrice(Integer.parseInt(new String(ch,start,length)));
        vPrice=false;
    }
    }
}
