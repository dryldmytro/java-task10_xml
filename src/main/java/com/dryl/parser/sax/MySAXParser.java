package com.dryl.parser.sax;

import com.dryl.model.Voucher;
import org.xml.sax.SAXException;


import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MySAXParser {
    private static SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
    public static List<Voucher> parse(File file) throws ParserConfigurationException, SAXException, IOException {
        List<Voucher> list = new ArrayList<>();
        SAXParser saxParser = saxParserFactory.newSAXParser();
        MySaxHandler saxHandler = new MySaxHandler();
        saxParser.parse(file,saxHandler);
        list = saxHandler.getVouchers();
        return list;
    }
}
